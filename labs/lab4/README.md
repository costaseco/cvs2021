# CVS - Fourth Lab Assignment (Introduction to Dafny)

- Date: 2021-04-15
- Expected Duration: <2h

This is an assignment using Dafny as in the specification, verification, and programming of Abstract Data Types.

You should read the lecture notes (`notes5.pdf`) to help with these exercises.

1. Implement a set of positive integers. Use the set classes `Set.dfy` and `SetAbs.dfy` and modify all operations so that the specification denotes the property of having only positive numbers. Produce client code that correctly connects to your new `PositiveSet` class.  

2. Implement the bank account described in file `SavingsAccount` with a pair of balances. One for the savings balance and another for the checking balance.