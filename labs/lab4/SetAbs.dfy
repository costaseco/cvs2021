class Set {
    
    var store:array<int>;
    var nelems: int;

    ghost var s:set<int>;

    function RepInv():bool
    reads store, `store, `nelems
    {
        0 < store.Length 
        && 0 <= nelems <= store.Length 
        && forall i,j :: 0 <= i < j < nelems ==> store[i] != store[j]
    }

    function Sound():bool
        reads `store, store, `nelems, `s
        requires RepInv()
    { forall x :: (x in s) <==> exists p:: (0<=p<nelems) && (store[p] == x) }

    function AbsInv():bool
        reads `store, store, `nelems, `s
    { RepInv() && Sound() }

    // the construction operation
    constructor(n: int)
    requires 0 < n
    ensures AbsInv() && s == {}
    {
        store := new int[n];
        nelems := 0;
        s := {};
    }

    // returns the number of elements in the set
    function size():int
        requires AbsInv()
        ensures AbsInv()
        reads store, `store, `nelems, `s
    { nelems }

    // returns the maximum number of elements in the set
    function maxSize():int
        requires AbsInv()
        reads store, `store, `nelems, `s
    { store.Length }

    method find(x:int) returns (r:int)
        requires AbsInv()
        ensures AbsInv()
        ensures r < 0 ==> forall j::(0<=j<nelems) ==> x != store[j];
        ensures r >=0 ==> r < nelems && store[r] == x;
    {
        var i:int := 0;
        while (i<nelems)
            decreases nelems-i
            invariant 0<=i<=nelems;
            invariant forall j::(0<=j<i) ==> x != store[j];
        {
            if (store[i]==x) { return i; }
            i := i + 1;
        }
        return -1;
    } 

    // checks if the element given is in the set
    method contains(v:int) returns (b:bool)
        requires AbsInv()
        ensures AbsInv() && b <==> v in s
    { 
        var i := find(v); 
        return i >= 0 ;
    }

    // adds a new element to the set if space available
    method add(v:int) 
        requires AbsInv()
        requires nelems < store.Length
        ensures AbsInv() && s == old(s) + {v}
        modifies store, `nelems, `s
    {
        var f:int := find(v);
        if (f < 0) {
            store[nelems] := v;
            nelems := nelems + 1;
            s := s + {v};
            assert forall i :: (0 <= i < nelems-1) ==> (store[i] == old(store[i]));
        }
    }
}

