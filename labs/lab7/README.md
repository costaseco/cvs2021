
# Lab 7

In this lab we will use verifast to verify the correctness of two ADTs. 

* Implement in Java and specify and verify it using `verifast` the classes __BCounter__ and __Queue__ which represents a bounded counter and a queue respectively. Further detail can be found in the each one of the incomplete java files provided in this folder.

* Install the spec __java.util.concurrent.locks.javaspec__ for the package __java.util.concurrent.locks__ by copying it to the __bin/rt/__ folder in your local verifast instalation and extend the __rt.jarspec__ file with the name of the previous file.

* Implement the concurrent version of these classes using monitors.


# Links 

[Verifast Github repository](https://github.com/verifast/verifast)

# Equipa docente

João Costa Seco
Bernardo Toninho

