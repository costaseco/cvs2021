

# CVS - First Lab Assignment (Testing and Coding)

- Date: 2021-03-18
- Expected Duration: <2h

This is a simple assignment requirement no new knowledge from this course, acting
as a form of review from other programming-related courses.

## Bootstrapping

First, to determine if your machine/IDE is working correctly with JUnit, check that test ``testId`` is passing
appropriately.


## Testing Max

Complete the tests in ``Lab1Tests.java`` for the ``max`` method, following the ``TODOs`` in each test.
You may want to use the JUnit ``assertThrows`` method to test the error cases.

## Testing Higher-Order Methods

Complete the tests in ``Lab1Tests.java`` for the ``allArray``, ``existsArray`` and ``mapInPlace`` methods,
following the ``TODOs`` in each test.

## Foos and Bars

Implement the ``fooBar`` method in ``Lab1.java``, following the informal specification. 
Write a test that exercises the specification and validates your implementation.






