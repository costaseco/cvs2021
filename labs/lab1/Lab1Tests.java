import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Lab1Tests {

    @Test
    void testIdentity() {
        // TODO: use this test to see if JUnit is properly set-up
        for(int i = -1000; i < 1000; i++)
            assertEquals( Lab1.identity(i), i);
    }

    @Test
    void testMaxArray() {
        assertEquals(1, Lab1.max(new int[]{1,2,3,4,5,6},1));
        assertEquals(2, Lab1.max(new int[]{1,2,3,4,5,6},2));
        assertEquals(4, Lab1.max(new int[]{1,2,3,4,5,6},4));
        assertEquals(6, Lab1.max(new int[]{1,2,3,4,5,6},6));
        assertEquals(-1, Lab1.max(new int[]{-1,-2,-3,-4,-5,-6},1));
        assertEquals(-1, Lab1.max(new int[]{-1,-2,-3,-4,-5,-6},2));
        assertEquals(-1, Lab1.max(new int[]{-1,-2,-3,-4,-5,-6},4));
        assertEquals(-1, Lab1.max(new int[]{-1,-2,-3,-4,-5,-6},6));
    }

    @Test
    void testMaxNullArray() {
        // TODO : implement a test that uses a null array
        fail();
    }

    @Test
    void testMaxZeroElements() {
        // TODO : implement a test the uses the method with an invalid number of elements (zero) to compare
        fail();
    }

    @Test
    void testMaxTooManyElements() {
        // TODO : implement a test the uses the method with an invalid number of elements (greater than
        //  the size of the array) to compare
        fail();
    }

    @Test
    void testAllArray() {
        // TODO: implement one or more tests for the allArray method. Fix any potential problems in the
        // given implementation.
        fail();
    }

    @Test
    void testExistsArray() {
        // TODO: implement one or more tests for the existsArray method. Fix any potential problems in the
        // given implementation.
        fail();
    }

    @Test
    void testMapInPlace() {
        // TODO: implement one or more tests for the mapInPlace method. Fix any potential problems in the
        // given implementation.
        fail();
    }

    @Test
    void testFooBar() {
        // TODO: test function fooBar according to the specification given in class Lab1
        fail();
    }

}
