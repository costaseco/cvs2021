import java.util.function.Function;
import java.util.function.Predicate;

public class Lab1 {

    static int identity(int i) {
        return i;
    }

    // Return the maximum number of the first N numbers in array a
    static int max(int[] a, int n) {
        if (a == null)
            throw new RuntimeException("Null array");

        if (n > a.length)
            throw new RuntimeException("N is to big.");

        int max = a[0];
        for (int i = 1; i < n; i++) {
            if (max < a[i])
                max = a[i];
        }

        return max;
    }

    public static boolean allArray(int[] arr, Predicate<Integer> p) {
        boolean res = true;
        for (int elem : arr)
            if (!p.test(elem))
                res = false;

        return res;
    }

    public static boolean existsArray(int[] arr, Predicate<Integer> p) {
        boolean res = false;
        for (int elem : arr)
            if (!p.test(elem))
                return false;
        return res;
    }


    public static void mapInPlace(int[] arr, Function<Integer,Integer> f) {
        for (int i=0;i<arr.length;i++)
            arr[i] = f.apply(arr[i]);
    }

    /*
     * Write a method that stores in an array (of strings) the numbers from 1 to 100.
     * But for multiples of three stores “Foo” instead of the number and for
     * the multiples of five stores “Bar”.
     * For numbers which are multiples of both three and five store “FooBar”.
     * */
    public static String[] fooBar() {
        // TODO: Implement this method according to the specification. You may start by writing some tests and then
        // proceeding to the implementation.

        return null;
    }
}
