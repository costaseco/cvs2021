public class Account {

  int checking;
  int savings;

  /*@ 
  predicate AccountInv(int c, int s) = 
        this.checking |-> c 
    &*& this.savings |-> s 
    &*& c >= 0 
    &*& s >= 0 
    &*& c >= -(s/2);
  @*/

  public Account()
  //@ requires true;
  //@ ensures AccountInv(0, 0);
  {
    savings = 0;
    checking = 0;
  }

  void deposit(int v) {} 

  void withdraw(int v) {}
  
  void save(int v) {}

  void rescue(int v) {}
  
  int getSavings()  { return 0; }
  
  int getChecking() { return 0; }
  
  public static void transfer(SavingsAccount a1, SavingsAccount a2, int v) {}
  
  public static void main(String[] args) {}
}
