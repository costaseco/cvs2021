# CVS - Second Lab Assignment (Introduction to Dafny)

[Setup instructions for Dafny](https://github.com/dafny-lang/dafny/wiki/INSTALL#Visual-studio-code)

- Date: 2021-03-24
- Expected Duration: <2h

This is an assignment using Dafny as a specification, verification, and programming language. You will learn the basic syntax of the language and the principles behind writing the strongest precondition of a method.

1. Implement, specify and verify the following functions. Define the strongest post-condition and the weakest pre-condition possible.
  Notice that a method with pre- and post-conditions is the same as a triple.
  
```
method Abs(x: int) returns (y: int)

method Min2(x: int, y:int) returns (w:int)

method Max2(x: int, y:int) returns (w:int)
```

  In this last case, use a function to specify the post-condition. 
  
  For the next case, use the function and methods above to specify and implement method `Max3`.

```
method Max3(x: int, y:int, z:int) returns (w:int)

method CompareTo(x:int, y:int) returns (c:int)
```

  Write as many intermediate assertions in the code as you can, to illustrate the proof behind a verified method in Dafny.

2. Implement, specify, and verify the following methods. Specify and verify the strongest post-condition possible. 

    a. A recursive version of a method that adds all the values from `0` to `n`. Start with the method prototype 

   Now, try to use the following function in the spec `function sum(n:int): int { n * (n+1) / 2 }`

```
method Sum(n:int) returns (res:int)
```

   Justify Dafny's reasoning using intermediate assertions in the code.

b. A method that implements the quadradic equation. The method should return a tuple capable of representing 0, 1, or 2 solutions. Note that dafny does not provide a square root function, but if you define a prototype for it

```
method sqrt(n:real) returns (m:real)
    ensures m*m == n
```

you can specify and verify the quadratic equation.

3. Consider the following Dafny methods:

```
method mistery1(n:nat,m:nat) returns (res:nat) 
   ensures true
{
   if (n==0) {
         return m;
   } else {
      var aux := mistery1 (n-1,m);
      return 1+aux;
   }
 }
 
method mistery2(n:nat,m:nat) returns (res:nat) 
   ensures true
{
   if (n==0) {
      return 0;
   }
   else {
      var aux := mistery2(n-1,m);
      var aux2 := mistery1(m,aux);
      return aux2;
   }
}
```

Change the post-condition of both methods so that it is the *strongest* possible.
