# CVS - Second Lab Assignment (Introduction to Dafny)

- Date: 2021-04-08
- Expected Duration: <2h

This is an assignment using Dafny as in the specification, verification, and programming of algorithms using arrays. You will learn how to use quantifiers to express properties about the elements of an array.

You should read the lecture notes (`notes4.pdf`) to help with these exercises.

1. Specify and implement method `fillK(a,n,k,c)`. This method returns `true` if and only if the first `c` elements, up to `n`, of array `a` are equal to `k`. 
        
    Define the weakest pre-condition and the strongest post-condition possible. Implement the method so that it verifies.

```
method fillK(a:array<int>, n:int, k:int, c:int) returns (b:bool)  
```


2. Specify and implement the method `containsSubString`. This method tests whether or not the array of characters `a` contains the elements of array `b`. If `a` contains `b`, then the method returns the offset of `b` in `a`. 
    
    If `a` does not contain `b` then the method returns an illegal index (e.g. `-1`).

    Define the weakest pre-condition and the strongest post-condition possible. Implement the method so that it verifies.

    Hint: you may want to define auxiliary functions and methods.

```
method containsSubString(a:array<char>, b:array<char>) returns (pos:int)
```

3. Specify and implement the method `resize`. This method returns a new array whose length is double of the length of the array given as argument (`a`). If the length of the array supplied as an argument is zero, then set the length of the resulting array (`b`) to a constant of your choice.

    All the elements of array `a` should be inserted, in the same order, in array `b`.

    Define the weakest pre-condition and the strongest post-condition possible. Implement the method so that it verifies.

```
method resize(a:array<int>) returns (z:array<int>)
```

4.  Specify and implement method `reverse`. This method receives an array `a` and returns a new array (`b`) in which the elements of a appear in the inverse order.  
  
    For instance, the inverse of array `a == [0, 1, 5, *, *]`, where `'*'` denotes an uninitialized array position, results in `b == [5, 1, 0, *, *]`.

    Define the weakest pre-condition and the strongest post-condition possible. Implement the method so that it verifies.

```
method reverse(a:array<int>, n:int) returns (z:array<int>)
```


